from django.db import models
from django.urls import reverse

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)

class Hat(models.Model):
    fabric = models.CharField(max_length=50)
    style = models.CharField(max_length=50)
    hat_primary_color = models.CharField(max_length=30)
    picture_url = models.URLField(null=True)
    
    location = models.ForeignKey(
        LocationVO,
        related_name="+",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
    
    class Meta:
        ordering = ("fabric", "style", "hat_primary_color", "picture_url", "location")