function HatsList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Closet</th>
                    <th>Hat Fabric</th>
                    <th>Hat Style</th>
                    <th>Hat Color</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.location.closet_name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.style}</td>
                            <td>{hat.hat_primary_color}</td>
                            <td></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default HatsList;