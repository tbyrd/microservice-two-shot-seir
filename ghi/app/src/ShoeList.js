function ShoeList(props) {
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model Name</th>
                    <th>Manufacturer</th>
                    <th>Color</th>
                    <th>Picture URL</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.href}>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.picture_url}</td>
                            <td>{shoe.bin.id}</td>
                            <td></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoeList;