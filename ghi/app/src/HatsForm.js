import React, { useEffect, useState } from 'react';

function HatsForm() {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        fabric: '',
        style: '',
        hat_primary_color: '',
        picture_url: '',
        location: '',
    })

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8090/api/hats/';

        const fetchConfig = {
            method: "post",
            //Because we are using one formData state object,
            //we can now pass it directly into our request!
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            //The single formData object 
            //also allows for easier clearing of data
            setFormData({
                fabric: '',
                style: '',
                hat_primary_color: '',
                picture_url: '',
                location: '',
            });
        }

    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        //We can condense our form data event handling
        //into on function by using the input name to update it

        setFormData({
            //Previous form data is spread (i.e. copied) into our new state object
            ...formData,

            //On top of the that data, we add the currently engaged input key and value
            [inputName]: value
        });
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Fabric" required type="text" name="fabric" id="id" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Color" required type="text" name="hat_primary_color" id="hat_primary_color" className="form-control" />
                            <label htmlFor="hat_primary_color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="ends">Picture Url</label>
                        </div>

                        <div className="mb-3">
                            <select onChange={handleFormChange} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.id}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>

                    </form>
                </div>
            </div>
        </div>
    )
}
export default HatsForm;