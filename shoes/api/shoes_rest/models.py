from django.db import models
from django.urls import reverse
# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=200) #Get the Bin Name
    import_href = models.CharField(max_length=200, unique=True) #Get the Bin URL


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200) 
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="bins",
        on_delete=models.PROTECT,
    )
    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"id": self.id})

    def __str__(self):
        return self.name