from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Shoe, BinVO
import json

# Create your views here.

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["id", "closet_name", "import_href"]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "bin", "id"]
    encoders = { 
        "bin": BinVOEncoder(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url"]
    encoders = {
        "bin": BinVOEncoder(),
    }
    def get_extra_data(self, o):
        return super().get_extra_data(o)


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )
    else:  
        content = json.loads(request.body) 
        print("Content", content)
    try:
        bin = BinVO.objects.get(id=content["bin"]) 
        content["bin"] = bin 
    except BinVO.DoesNotExist:  
        return JsonResponse({"message": "Bin Does Not Exist"},status=400,) 
    shoe = Shoe.objects.create(**content) 
    return JsonResponse(
        shoe,
        encoder=ShoeDetailEncoder,
        safe=False,
    )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, id):
    if request.method == "GET": 
        shoe = Shoe.objects.get(id=id) 
        return JsonResponse( 
            shoe, 
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE": 
        count, _ = Shoe.objects.filter(id=id).delete() 
        return JsonResponse({"deleted": count > 0}) 
    else: 
        content = json.loads(request.body) 
        try:
            if "bin" in content: 
                bin = BinVO.objects.get(closet_name=content["bin"]) 
                content["bin"] = bin 
        except BinVO.DoesNotExist: 
            return JsonResponse(
                {"message": "Bin Does Not Exist"}, 
                status=400,
            )
        Shoe.objects.filter(id=id).update(**content) 
        shoe = Shoe.objects.get(id=id) 
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )